import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JTextArea;

public class Model {
	protected LinkedList<Queue> cozi;
	protected JTextArea ta = new JTextArea();
	protected static Timer timer = new Timer();
	Timer afisareTimer;
	protected View theView;
	protected int minSvTime = 2, maxSvTime = 8, nrOfQueues, minArrive, maxArrive, time = 0;
	protected boolean runFlag=true;
	protected static int totalTimeService=0,totalClienti=0,clientiCurenti=0,max=-1000;
	

	static void decrementClientiCurenti()
	{
		clientiCurenti--;
	}
	
	static void addToTotalTimeService(int p)
	{
		totalTimeService +=p;
	}
	
	
	public Model(View theView) {
		this.theView = theView;
	}

	public void initModel(int nrOfQueues, int minService, int maxService, int minArrive, int maxArrive, int time) {
		// init theModel with the values from the GUI
		this.nrOfQueues = nrOfQueues;
		this.minSvTime = minService;
		this.maxSvTime = maxService;
		this.minArrive = minArrive;
		this.maxArrive = maxArrive;
		this.time = time;

		// creating the queues
		cozi = new LinkedList<Queue>();
		for (int i = 0; i < nrOfQueues; i++) {
			cozi.add(new Queue(theView,i,this));
		}

		// start timer for generating a new client
		new Task().run();

		afisareTimer = new Timer(); // create a new timer
		TaskAfisare st = new TaskAfisare(); // create a new task for afisare
											// Instantiate SheduledTask class
		afisareTimer.schedule(st, 0, 100); // Create Repetitively task for
											// every 1 secs
}
	
	public void terminateModel()
	{
		for(Queue element:  cozi)
			element.terminate();
		try {
			theView.getTextArea().append("totalClienti="+totalClienti+"\ntotalTimeService="+totalTimeService+"\n");
			theView.getTextArea().append("timp mediu de asteptare="+(totalTimeService/totalClienti));
			timer.cancel();
			afisareTimer.cancel();
		} catch (NullPointerException e) {
			// TODO: handle exception
		}
		
	}

	class TaskAfisare extends TimerTask {
		@Override
		public void run() {
			// TODO Auto-generated method stub
			System.out.println("Starea cozilor:");
			int i = 0;
			for (Queue element : Model.this.cozi) {
				System.out.print("Coada" + i + ":");
				// System.out.print(element.getNumberOfClients());
				for (Client client : element.getClients()) {
					System.out.print(client.getServiceTime() + " ");
					// System.out.print();
				}
				i++;
				System.out.println();
			}
			System.out.println("");

		}
	}

	// task class
	class Task extends TimerTask {
		@Override
		public void run() {
			
			int delay = (Model.this.minArrive + new Random().nextInt(Model.this.maxArrive - Model.this.minArrive))
					* 1000;
			timer.schedule(new Task(), delay);
			// add a new client at the shortes queue
			// System.out.println("cea mai scurta coada:"+getShortestQueueNb());
			if(Model.this.getLongestQueueNb()>max)
			{
				max = Model.this.getLongestQueueNb();
				System.out.println("cea mai lunga queue"+Model.this.getLongestQueueNb());
			}
				
			clientiCurenti++;
			totalClienti++;
			theView.getTextArea().append("Adaugam un client la coada "+Model.this.getShortestQueueNb()+"\n");
			Model.this.getShortestQueue().getClients().add(new Client(Model.this.minSvTime, Model.this.maxSvTime));
			// System.out.println(new Date());
		}
	}

	ArrayList<Integer> getDimensiuniCozi() {
		ArrayList<Integer> dimensiuniCozi = new ArrayList<Integer>();

		for (Queue element : cozi)
			dimensiuniCozi.add(element.getNumberOfClients());
		return dimensiuniCozi;
	}
	
	
	

	// getters and setters
	Queue getShortestQueue() {
		int min = 100;
		int i = 0;
		int poz = 0;
		Queue q = cozi.getFirst();
		for (i = 0; i < nrOfQueues; i++) {
			if (cozi.get(i).getNumberOfClients() < min) {
				min = cozi.get(i).getNumberOfClients();
				poz = i;
			}
		}
		return cozi.get(poz);
	}

	int getShortestQueueNb() {
		int min = 100;
		int i = 0;
		int poz = 0;
		Queue q = cozi.getFirst();
		for (i = 0; i < nrOfQueues; i++) {
			if (cozi.get(i).getNumberOfClients() < min) {
				min = cozi.get(i).getNumberOfClients();
				poz = i;
			}
		}

		return poz;
	}
	
	int getLongestQueueNb()
	{
	int max = 100;
	int i = 0;
	int poz = 0;
	Queue q = cozi.getFirst();
	for (i = 0; i < nrOfQueues; i++) {
		if (cozi.get(i).getNumberOfClients() >max) {
			max = cozi.get(i).getNumberOfClients();
			poz = i;
		}
	}

	return poz;
		
	}
	
	

	LinkedList<Queue> getCozi() {
		return this.cozi;
	}

	int getNrCozi() {
		return this.cozi.size();
	}

}
