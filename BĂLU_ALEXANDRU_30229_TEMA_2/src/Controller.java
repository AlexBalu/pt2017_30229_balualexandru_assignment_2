import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;
import java.util.TimerTask;

public class Controller implements Runnable {
private Model theModel;
private View theView;
private Thread t;
private boolean threadFlag=true;
private Timer timer = new Timer();

public Controller(Model model, View view)
{
theModel = model;
theView  = view;
theView.addActionListener(new GetDataAndDraw());
t = new Thread(this);

}

public void terminateController()
{
	threadFlag=false;
}

public void run() {
	while(threadFlag)
	{
		try {
	
			theView.drawQueues(theModel.nrOfQueues, theModel.getDimensiuniCozi(),theModel.getCozi());
			t.sleep(100);
		} catch (InterruptedException e) {
			System.out.println("got interrupted!");
		}
	}
}

class GetDataAndDraw implements ActionListener
{
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
		//get data from the view to set init values;
		int nQueues = theView.getNQues();
		int minServiceTime= theView.getMinService();
		int maxServiceTime= theView.getMaxService();
		int minArrive =  theView.getMinArrive();
		int maxArrive =  theView.getMaxArrive();
		int time= theView.getTime();
		
		
		new java.util.Timer().schedule( 
		        new java.util.TimerTask() {
		            @Override
		            public void run() {
		            	theModel.terminateModel();
		                Controller.this.terminateController();
		            }
		        }, 
		        theView.getTime()*1000);
		
		
		theModel.initModel(nQueues, minServiceTime, maxServiceTime, minArrive,maxArrive, time);
		//theView.drawQueues(theModel.getNrCozi(), theModel.getDimensiuniCozi());
		t.start();

		
	}

}

}
