import java.util.LinkedList;

public class Queue implements Runnable {
	volatile LinkedList<Client> clients;
	Thread t;
	private View theView;
	private Model theModel;
	private int id;
	private boolean threadRun=true;
	
	public void terminate()
	{
		this.threadRun=false;
	}
	
	public Queue(View theView,int id,Model theModel) {
		this.theView = theView;
		this.theModel = theModel;
		clients = new LinkedList<Client>();
		t = new Thread(this);
		t.start();
		this.id = id;
		id++;

	}

	@Override
	public void run() {
		while (threadRun) {
			// TODO Auto-generated method stub
			try {
				//System.out.println("clients size="+clients.size());
				if (this.clients.size() > 0) {
					//System.out.println("Eliminam un client");
					Model.decrementClientiCurenti();
					t.sleep(clients.getFirst().getServiceTime() * 1000);
					theView.getTextArea().append("\nEliminam un client de la coada "+this.id);
					clients.remove(clients.getFirst());
				}
			} catch (InterruptedException e) {
				System.out.println("got interrupted!");
			}
		}
	}

	// gettere si settere
	public int getNumberOfClients() {
		return clients.size();
	}

	synchronized LinkedList<Client> getClients() {
		return this.clients;
	}
	
	

}
