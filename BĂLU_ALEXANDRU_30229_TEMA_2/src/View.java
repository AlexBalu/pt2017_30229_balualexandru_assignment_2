import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class View extends JFrame {
	private JTextField minArrive= new JTextField("1");
	private JTextField maxArrive = new JTextField("3");
	private JTextField minService = new JTextField("3");
	private JTextField maxService = new JTextField("5");
	private JTextField nQueues = new JTextField("3");
	private JTextField time = new JTextField("15");
	private JButton button =  new JButton("START");
	private JTextArea ta = new JTextArea("Logging Area:\n ");
	private JScrollPane scroll = new JScrollPane(ta);
	private JPanel panel = new JPanel();
	private JPanel mainP = new JPanel();
	private JPanel  p = new JPanel();
	View()
	{	
		scroll.setPreferredSize(new Dimension(100, 100));
		JPanel p0 = new JPanel();
		p0.setLayout(new BorderLayout());
		p0.add(scroll);
		mainP.add(p0);
		
		JPanel p1 = new JPanel();
		p1.setLayout(new FlowLayout());
		p1.add(new JLabel("min-max between arriving:"));
		p1.add(minArrive);
		p1.add(maxArrive);
		mainP.add(p1);
		
		JPanel p2 = new JPanel();
		p2.setLayout(new FlowLayout());
		p2.add(new JLabel("min-max Service:"));
		p2.add(minService);
		p2.add(maxService);
		p2.setSize(150,150);
		mainP.add(p2);
				
		
		JPanel p3 = new JPanel();
		p3.setLayout(new FlowLayout());
		p3.add(new JLabel("Numar cozi:"));
		p3.add(nQueues);
		mainP.add(p3);
		
		JPanel p4 = new JPanel();
		p4.setLayout(new FlowLayout());
		p4.add(new JLabel("time:"));
		p4.add(time);
		mainP.add(p4);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(1200, 700);
		
		mainP.add(button);
		mainP.add( Box.createRigidArea(new Dimension(0,100)) );
		this.setSize(900,900);
		p2.setVisible(true);
		mainP.setLayout(new BoxLayout(mainP, BoxLayout.Y_AXIS));
		p.add(mainP);
		this.add(p);
		//this.add(mainP);
		this.setVisible(true);
		
		
	}
	
	//important
	void addActionListener(ActionListener e)
	{
		button.addActionListener(e);
	}
	
	int getMinArrive()
	{
		return Integer.parseInt(this.minArrive.getText());
	}
	
	int getMaxArrive()
	{
		return Integer.parseInt(this.maxArrive.getText());
	}
	
	int getMinService()
	{
		return Integer.parseInt(this.minService.getText());
	}
	
	int getMaxService()
	{
		return Integer.parseInt(this.maxService.getText());
	}
	
	int getNQues()
	{
		return Integer.parseInt(this.nQueues.getText());
	}
	
	int getTime()
	{
		return Integer.parseInt(this.time.getText());
	}

	JTextArea getTextArea()
	{
		return this.ta;
	}
	
	private JButton createGridButton(int time) {
			Random ran = new Random();
			int x = ran.nextInt(3 )+1;
			
	    	ImageIcon image = new ImageIcon("client1.png");
	    	JButton b = new JButton(Integer.toString(time),image);
	    	return b;
	}    	
	

	void drawQueues(int nQ,ArrayList<Integer> dimensiuniCozi,LinkedList<Queue> cozi)
	{
		panel.removeAll();
		
		//get max queue
		int max = -500;
		for(Integer element:dimensiuniCozi)
		{
			if( element.intValue() > max)
				max = element.intValue();
		}
		
	
		panel.setLayout(new GridLayout(nQ,max ));
		for(int i=0;i<nQ;i++)
			for(int j=0;j<max;j++)
			{	
				try {
					JButton gb = createGridButton(cozi.get(i).getClients().get(j).getServiceTime());
					 panel.add(gb);
				} catch (IndexOutOfBoundsException e) {
					// TODO: handle exception
					Component rigidArea = Box. createRigidArea(new Dimension(10, 10));
					panel.add(rigidArea);
				}
				/*
				if(j >= dimensiuniCozi.get(i))
				{
					Component rigidArea = Box. createRigidArea(new Dimension(10, 10));
					panel.add(rigidArea);
				}
				else
				{
				 JButton gb = createGridButton(cozi.get(i).getClients().get(j).getServiceTime());
				 panel.add(gb);
				}*/
			}
		//panel.setVisible(true);
		mainP.add(panel);
		this.validate();
		
		this.repaint();
		
	}
	
	
}
